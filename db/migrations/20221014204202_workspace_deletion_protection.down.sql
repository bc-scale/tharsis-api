ALTER TABLE workspaces
    DROP COLUMN IF EXISTS prevent_destroy_plan;
